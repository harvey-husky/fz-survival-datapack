# 这个文件并不会自动执行，有需要可以手动执行
carpet setDefault language zh_cn
carpet setDefault stackableShulkerBoxes 16
carpet setDefault leadFix true
carpet setDefault ctrlQCraftingFix true
carpet setDefault smoothClientAnimations true
carpet setDefault fastRedstoneDust true
carpet setDefault reloadSuffocationFix true
carpet setDefault persistentParrots true
carpet setDefault missingTools true
carpet setDefault optimizedTNT true
carpet setDefault placementRotationFix true
carpet setDefault cleanLogs true
carpet setDefault lightningKillsDropsFix true
carpet setDefault lagFreeSpawning true
carpet setDefault accurateBlockPlacement true
carpet setDefault flippinCactus true
carpet setDefault defaultLoggers mobcaps,tps
